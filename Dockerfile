ARG SCANNER_VERSION=v1.4.5

FROM alpine:latest

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

# should be "gitlab", per https://about.gitlab.com/handbook/engineering/development/secure/#dockerfile
USER root

ENTRYPOINT []
CMD ["pwd"]
